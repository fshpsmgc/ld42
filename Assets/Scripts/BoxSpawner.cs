﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour {
    public float MaxSpawnTime;
    public float MinSpawnTime;
    public Vector2 SpawnForce;
    float NextSpawnTime;
    float TimePassed;
    public GameObject Box;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        TimePassed += Time.deltaTime;
        if(TimePassed >= NextSpawnTime)
        {
            Spawn();

            TimePassed = 0;
            NextSpawnTime = Random.Range(MinSpawnTime, MaxSpawnTime);
        }
	}

    void Spawn()
    {
        GameObject tmp = Instantiate(Box, transform.position, transform.rotation);
        tmp.GetComponent<Rigidbody2D>().AddForce(SpawnForce);
    }
}
