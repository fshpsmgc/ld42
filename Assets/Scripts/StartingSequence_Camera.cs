﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartingSequence_Camera : MonoBehaviour {
    public float MovementSpeed;
    public float SizeSpeed;
    public Canvas HUD;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, 150, SizeSpeed);
        transform.position = Vector2.Lerp(transform.position, Vector2.zero, MovementSpeed);
        transform.Translate(0, 0, -10);
        print((Vector2.zero - (Vector2)transform.position).magnitude);
        if ((Vector2.zero - (Vector2)transform.position).magnitude < 60)
        {
            GetComponent<FollowCamera>().enabled = true;
            enabled = false;
            HUD.enabled = true;
        }
    }
}
