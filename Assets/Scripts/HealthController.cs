﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour {
    public float VelocityThreshold;
    Rigidbody2D body;
    SpriteRenderer sprite;
    public int Health;
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.tag == "Environment")
        {
            print("Collision velocity: " + body.velocity.magnitude);
            if (body.velocity.magnitude >= VelocityThreshold)
            {
                ReduceHealth();
            }
        }
    }

    void ReduceHealth()
    {
        Health--;
        if(Health == 0)
        {
            Destroy(gameObject);
        }
    }
}
