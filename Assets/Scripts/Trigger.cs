﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class Trigger : MonoBehaviour {
    public enum Activator
    {
        Player,
        Box,
        Anything
    }
    public Activator ActivatorType;
    public UnityEvent OnEnter;
    public bool DestroyOnTrigger;
    public bool DestroyActivator;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(ActivatorType != Activator.Anything)
        {
            switch (other.tag)
            {
                case "Player":
                    if (ActivatorType == Activator.Player)
                    {
                        OnEnter.Invoke();
                        if (DestroyOnTrigger)
                            Destroy(gameObject);
                        if (DestroyActivator)
                            Destroy(other.gameObject);
                    }
                    break;
                case "Box":
                    if (ActivatorType == Activator.Box)
                    {
                        OnEnter.Invoke();
                        if (DestroyOnTrigger)
                            Destroy(gameObject);
                        if (DestroyActivator)
                            Destroy(other.gameObject);
                    }
                    break;
            }
        }else
        {
            OnEnter.Invoke();
            if (DestroyOnTrigger)
                Destroy(gameObject);
            if (DestroyActivator)
                Destroy(other.gameObject);
        }        
    }
}
