﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameController : MonoBehaviour {
    public float RouteLength;
    public float RouteCovered;

    public float MaxFuel;
    public float CurrentFuel;
    public float FuelConsumption;

    public float MaxShields;
    public float CurrentShields;

    public float Happiness;
    public float HappinessDecrease;

    public Slider RouteSlider;
    public Slider ShieldSlider;
    public Slider FuelSlider;
    public Slider HappinessSlider;

    [Range(0, 100)]
    public float HitChance;

    public float HitDamageMax;
    public float HitDamageMin;

    public bool isEnded;
    public bool isStarted;
    float TimePassed;
    float SecondsPassed;

    public EndingSequence[] VictoryStation;
    // Use this for initialization
    void Start () {
        RouteSlider.maxValue = RouteLength;
        RouteSlider.value = RouteCovered;

        ShieldSlider.maxValue = MaxShields;
        ShieldSlider.value = CurrentShields;

        FuelSlider.maxValue = MaxFuel;
        FuelSlider.value = CurrentFuel;

        HappinessSlider.maxValue = Happiness;
        HappinessSlider.value = Happiness;
    }
	
    public void StartGame()
    {
        isStarted = true;
    }

	// Update is called once per frame
	void Update () {
        if (!isEnded && isStarted)
        {
            TimePassed += Time.deltaTime;
            Update_UI();

            if (TimePassed >= 1)
            {
                Happiness -= HappinessDecrease;
                CurrentFuel -= FuelConsumption;
                RouteCovered += CurrentFuel / MaxFuel;

                if(Random.Range(0,100) <= HitChance)
                {
                    Hit();
                }

                if (RouteCovered >= RouteLength)
                {
                    Victory();
                }

                TimePassed = 0;
                SecondsPassed++;
            }
        }
	}

    void Update_UI()
    {
        RouteSlider.value = RouteCovered;
        FuelSlider.value = CurrentFuel;
        ShieldSlider.value = CurrentShields;
        HappinessSlider.value = Happiness;
    }

    public void AddMaxFuel(float value)
    {

    }

    public void AddFuel(float value)
    {
        CurrentFuel += value;
        if(CurrentFuel > MaxFuel)
        {
            CurrentFuel = MaxFuel;
        }
    }

    public void AddMaxShields(float value)
    {

    }

    public void AddShields(float value)
    {
        CurrentShields += value;
        if (CurrentFuel > MaxShields)
        {
            CurrentShields = MaxShields;
        }
    }

    public void AddHappiness(float value)
    {
        Happiness += value;
    }

    void Victory()
    {
        print("Victory");
        isEnded = true;
        foreach(EndingSequence seq in VictoryStation)
        {
            seq.StartSequence();
        }
        GameObject.Find("BoxSpawner").GetComponent<BoxSpawner>().enabled = false;
        Camera.main.GetComponent<FollowCamera>().enabled = false;
    }

    public void Hit()
    {
        CurrentShields -= Random.Range(HitDamageMin, HitDamageMax);
    }
}
