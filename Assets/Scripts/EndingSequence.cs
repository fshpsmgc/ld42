﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EndingSequence: MonoBehaviour{
    public Vector2[] Path;
    public float Z;
    public float[] Speed;
    int CurrentWaypoint;
    bool isStarted;
	// Use this for initialization
	void Start () {
		
	}

	public void StartSequence()
    {
        isStarted = true;
    }

	// Update is called once per frame
	void Update () {
        if (isStarted)
        {
            transform.position = Vector2.Lerp(transform.position, Path[CurrentWaypoint], Speed[CurrentWaypoint]);
            transform.Translate(0, 0, Z);

            if ((Path[CurrentWaypoint] - (Vector2)transform.position).magnitude < 0.1f)
            {
                if(CurrentWaypoint != Path.Length - 1)
                {
                    CurrentWaypoint++;
                }else
                {
                    print("Sequence Ended");
                }        
            }
        }

    }
}
