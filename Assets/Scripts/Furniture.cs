﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Furniture : MonoBehaviour {
    public float HappinessBonus;
    public float ShieldsBonus;
    public float FuelBonus;

    public float HUD_Timeout;
    public TMP_Text Info;
	// Use this for initialization
	void Start () {
        Destroy(Info.gameObject, HUD_Timeout);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Place()
    {
        GameController gc = GameObject.Find("GameController").GetComponent<GameController>();
        gc.Happiness += HappinessBonus;
        gc.FuelConsumption -= FuelBonus;
        gc.MaxShields += ShieldsBonus;

        if(HappinessBonus != 0)
        {
            Info.text += "Friend's Happiness: +" + HappinessBonus + "\n";
        }
        if(ShieldsBonus != 0)
        {
            Info.text += "Shields Power: +" + HappinessBonus + "\n";
        }
        if(FuelBonus != 0)
        {
            Info.text += "Fuel Efficiency: +" + HappinessBonus + "\n";
        }
    }
}
