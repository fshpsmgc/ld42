﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour {
    public enum Direction
    {
        Left,
        Right
    }

    public float MaxSpeed;
    public float Acceleration;
    public float JetpackForce;

    float Input_X;
    Rigidbody2D body;
    Animator anim;
    SpriteRenderer sprite;
    public Direction Facing;
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        Facing = Direction.Left;
	}
	
	// Update is called once per frame
	void Update () {
        Input_X = Input.GetAxis("Horizontal");

        if (Input.GetKey(KeyCode.LeftControl))
        {
            body.AddForce(new Vector2(0, -2 * JetpackForce));
            anim.Play("Player_Idle");
        }

        if (Input.GetKey(KeyCode.Space))
        {
            body.AddForce(new Vector2(0, JetpackForce));
            anim.Play("Player_Idle");
        }
        if(Input_X != 0)
        {
            if(Input_X > 0 && Facing == Direction.Left)
            {
                sprite.flipX = true;
                Facing = Direction.Right;
                if (GetComponentInChildren<GrabbableBox>() != null)
                    GetComponentInChildren<GrabbableBox>().transform.position = new Vector3(transform.position.x + 2 * GetComponent<SpriteRenderer>().bounds.extents.x, transform.position.y, 0);
            }

            if (Input_X < 0 && Facing == Direction.Right)
            {
                sprite.flipX = false;
                Facing = Direction.Left;
                if(GetComponentInChildren<GrabbableBox>() != null)
                    GetComponentInChildren<GrabbableBox>().transform.position = new Vector3(transform.position.x -2 * GetComponent<SpriteRenderer>().bounds.extents.x, transform.position.y, 0);
            }

            if (body.velocity.x < MaxSpeed)
            {
                body.AddForce(new Vector2(Acceleration * Input_X, 0));
                anim.Play("Player_Running");
            }
        }else
        {
            anim.Play("Player_Idle");
        }
    }
}
