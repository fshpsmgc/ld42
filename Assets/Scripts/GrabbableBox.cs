﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GrabbableBox : MonoBehaviour {
    Rigidbody2D body;
    bool isNearPlayer;
    Transform player;
    bool isAttached;

    public UnityEvent OnPickup;
    public UnityEvent OnDrop;
    public UnityEvent OnPlayerProximity;
    public UnityEvent OnPlayerProximityEnd;
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.E))
        {
             if (isNearPlayer)
             {
                 if (isAttached)
                 {
                     Detach();
                 }else
                 {
                     Attach(player);
                 }
             }else
             {
                 if (isAttached)
                 {
                     Detach();
                 }
             }            
        }

        if (Input.GetKeyDown(KeyCode.F) && isAttached)
        {
            OpenBox();
        }
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            isNearPlayer = true;
            player = other.transform;
            OnPlayerProximity.Invoke();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            isNearPlayer = false;
            player = null;
            OnPlayerProximityEnd.Invoke();
        }
    }

    public void Attach(Transform other)
    {
        PlayerMovement.Direction relativePos;
        if(other.position.x > transform.position.x)
        {
            relativePos = PlayerMovement.Direction.Right;
        }else
        {
            relativePos = PlayerMovement.Direction.Left;
        }

        if(other.GetComponent<PlayerMovement>().Facing != relativePos)
        {
            print("Attaching");
            if(relativePos == PlayerMovement.Direction.Right)
            {
                transform.position = new Vector3(other.transform.position.x - 2 * other.GetComponent<SpriteRenderer>().bounds.extents.x, other.transform.position.y, 0);
            }else
            {
                transform.position = new Vector3(other.transform.position.x + 2 * other.GetComponent<SpriteRenderer>().bounds.extents.x, other.transform.position.y, 0);
            }
            if(body != null)
            {
                body.simulated = false;
            }else
            {
                body = GetComponent<Rigidbody2D>();
                body.simulated = false;
            }
            transform.SetParent(other);
            isAttached = true;
            OnPickup.Invoke();
        }

    }

    public void Detach()
    {
        print("Detaching");
        body.simulated = true;
        if (transform.parent.position.x > transform.position.x)
        {
            body.AddForce(new Vector2(-5000, 2000));
        }
        else
        {
            body.AddForce(new Vector2(5000, 2000));
        }
        
        transform.SetParent(null);
        isAttached = false;
        OnDrop.Invoke();
    }

    public void OpenBox()
    {
        BoxController tmp = GameObject.Find("GameController").GetComponent<BoxController>();
        GameObject spawned = Instantiate(tmp.Deliveries[Random.Range(0, tmp.Deliveries.Length - 1)], transform.position, transform.rotation);
        spawned.GetComponent<GrabbableBox>().Attach(GameObject.FindGameObjectWithTag("Player").transform);
        Destroy(gameObject);
    }
}
